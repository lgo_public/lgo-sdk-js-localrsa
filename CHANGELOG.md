# Changelog

## 1.0.2 (unreleased)

todo

## 1.0.1 (2019-08-13)

### Changed

- Dependencies update and audit

## 1.0.0 (2019-06-03)

Initial release
