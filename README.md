# @lgo/sdk-localrsa

Local RSA CryptoKi plugin for [LGO SDK](https://gitlab.com/lgo_public/lgo-sdk-js).

## Installation

```
npm install @lgo/sdk-localrsa --save
```

## Examples

```typescript
import { Client } from '@lgo/sdk';
import { LocalRsaCryptoKi } from '@lgo/sdk-localrsa';

const myCryptoKi = new LocalRsaCryptoKi({
  privateKeyPath: '/the/path/to/my/key'
});

const client = new Client({
  cryptoKi: myCryptoKi,
  accessKey: '971432e9-746c-4c29-ab68-24704347a1e3'
});
```

## Api

To use this cryptoKi instanciate it as follows:

```typescript
import { LocalRsaCryptoKi } from '@lgo/sdk-localrsa';

new LocalRsaCryptoKi(/* options */);
```

You can also use require syntax:

```javascript
const { LocalRsaCryptoKi } = require('@lgo/sdk-localrsa');

new LocalRsaCryptoKi(/* options */);
```

Options are provided as an `object` with following properties:

- `privateKey?: string` - PKCS#1 or PKCS#8 RSA private key in PEM format
- `privateKeyPath?: string` - File containing a PKCS#1 or PKCS#8 RSA private key in PEM format

Example:

```typescript
import { LocalRsaCryptoKi } from '@lgo/sdk-localrsa';

const myPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAke2/JycndCpo/T0DFU3kzQ2qGN7hq3bbyXTgp0qKIKs2Me4K
NPSvHsDg4UbWZjWWDvvaGkVwAc9ZDgjZVF79xKOEC63yn89jODUWlHDlcHJmIeXD
... truncated key
-----END RSA PRIVATE KEY-----`;

const cryptoKi = new LocalRsaCryptoKi({
  privateKey: myPrivateKey
});
```

## License

[MIT](LICENSE)
