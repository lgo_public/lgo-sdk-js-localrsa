import * as crypto from 'crypto';
import * as path from 'path';

import { LocalRsaCryptoKi } from './localRsaCryptoKi';

describe('Local rsa crypto ki', () => {
  describe('while signing', () => {
    it('should return signature as hexadecimal string', async () => {
      const cryptoKi = new LocalRsaCryptoKi({
        privateKey: createPkcs1PrivateKey()
      });

      const signature = await cryptoKi.sign('my message');

      expect(signature).toMatch(/^[0-9a-fA-F]+$/g);
    });

    it('should do the job with a pkcs#1 private key', async () => {
      const cryptoKi = new LocalRsaCryptoKi({
        privateKey: createPkcs1PrivateKey()
      });

      const signature = await cryptoKi.sign('my message');

      expect(verifySignature('my message', signature)).toBeTruthy();
    });

    it('should do the job with a pkcs#8 private key', async () => {
      const cryptoKi = new LocalRsaCryptoKi({
        privateKey: createPkcs8PrivateKey()
      });

      const signature = await cryptoKi.sign('my message');

      expect(verifySignature('my message', signature)).toBeTruthy();
    });

    it('could use a private key from a file', async () => {
      const cryptoKi = new LocalRsaCryptoKi({
        privateKeyPath: path.resolve(__dirname, 'test', 'privateKey.txt')
      });

      const signature = await cryptoKi.sign('my message');

      expect(verifySignature('my message', signature)).toBeTruthy();
    });

    it('should fail if private key is malformed', () => {
      const call = () => new LocalRsaCryptoKi({ privateKey: 3 } as any);

      expect(call).toThrow(
        'Expected property `privateKey` to be of type `string` but received type `number`'
      );
    });

    it('should fail if private key path is malformed', () => {
      const call = () => new LocalRsaCryptoKi({ privateKeyPath: 3 } as any);

      expect(call).toThrow(
        'Expected property `privateKeyPath` to be of type `string` but received type `number`'
      );
    });

    it('should fail if both private key and path are missing', () => {
      const call = () => new LocalRsaCryptoKi({} as any);

      expect(call).toThrow(
        'Either property `privateKey` or `privateKeyPath` must be defined'
      );
    });
  });
});

function verifySignature(message: string, signature: string): boolean {
  const verify = crypto.createVerify('SHA256');
  verify.write(message);
  verify.end();
  const buffer = Buffer.from(signature, 'hex');
  return verify.verify(createPublicKey(), buffer);
}

function createPublicKey() {
  return `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1+V1BWYp60lZODepAnkY
/1vBpCfTqIBS2/jNLqHyI9g1wd8IYnX7jDJou7oPVEUTFnlpADvexxOpCMNP4Glh
o2sb1Waojzf3NkhhHwfZ2i6ZFGytTKG0P8ob3gFw7LgdwbtreXYNiAA44i/hqror
eSyKeG6Cr1KSqH2XtlYEWAgU7ROHHN9c8meftPebp/XC77xfRIM6SOhbWwdWwpfY
vS3vIjSdu7XFAAOt+pfrJvPMrLrurEkrlg3d4acD/vnD7U0IfTKKv1FejTXpwY7v
IURvs0+JEH6fTpfer5CfMLY0Q9HeDaLCPukdpBGhaSCIMA3o2hdqheYmIQQi9ZF+
3wIDAQAB
-----END PUBLIC KEY-----`;
}

function createPkcs1PrivateKey() {
  return `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA1+V1BWYp60lZODepAnkY/1vBpCfTqIBS2/jNLqHyI9g1wd8I
YnX7jDJou7oPVEUTFnlpADvexxOpCMNP4Glho2sb1Waojzf3NkhhHwfZ2i6ZFGyt
TKG0P8ob3gFw7LgdwbtreXYNiAA44i/hqroreSyKeG6Cr1KSqH2XtlYEWAgU7ROH
HN9c8meftPebp/XC77xfRIM6SOhbWwdWwpfYvS3vIjSdu7XFAAOt+pfrJvPMrLru
rEkrlg3d4acD/vnD7U0IfTKKv1FejTXpwY7vIURvs0+JEH6fTpfer5CfMLY0Q9He
DaLCPukdpBGhaSCIMA3o2hdqheYmIQQi9ZF+3wIDAQABAoIBAQDOEqQHGkQq5GE3
6tD0nlhA7flAUg+Sx6HPe4WF3Gh+1FJDkjqzmMAZSshzX55gJmqYyhxyb7MuPFLd
HWhx4MSksojnwcxkn9vkwWOKtfi4bEALocws2DBvrt+4oZGDMh6dG+LzFb7GsMeb
Bm8Vggwa5mvpljpBVMTN9MtJ7ILK7nKAUkW6DqG5Tq0t6kWd3EdOy1p9l5wz2OUb
MCvWBn3dO82NMC8udrXiOhKNQXblbcWOdS0haEWPJNdvQ2yUPa1Qla0csnoiXSRH
pU37T9h/zV4qYHYz1BTv0ZYuPSa2OxbRjIKOr7C6wNbAg7mLvzzsW6+ZfxUhdeh3
N+cApJtxAoGBAPdkFAzSDF5/GZ+CcYFkPg5uHiyqR4N+/tz/2ED8bA5L6q3QkfCJ
T6xZfVwsc/CiCZvfWO99cgdGWOoYp/LVXJ5pjEaWoRIr4ca70b+Ev0thv6i5QKG3
/c2jjJcJ8NjGkXCihEAy/18Dex2mQVsqtqlln8DmFVOTzsyCnuu7nLL5AoGBAN9o
zdIB5WxEHiFHI7YCJk0uXyxubYT0JjqjkKbeijo5LHUQLsYy4uFasEm6xnZqwOLY
YVLAX2Tw3z6vDRaXJ29CNH3gEjoLcs6ZguJRvYX7QHmqC4/aZygTUPUBexIOlMK6
JqC+E4zmLbkB4jDPWtxXTwJNZlSTMUJk03r8cN6XAoGAWY3U5D81Uw/XirKBvEE5
ZaffLHQHp0ZVpFLJSd0WkKZSsdEKw3Qxb0aF5tQAtF8z80UKwMc8e/2vOi7qvTEX
chDrQrJMZHOWK4baCuZXCUa9RqqpBuG/8qz9u79RfGeFRqzOCx9q6k4sRzNj/Uid
Dv1Gs2GW3sZm2gl+1f74XqECf0huskxkUP3JF/uyFOHt0H01i6XcJ6XzgnGAlmWD
wU0fPEq39HJODUEDJiTBaZb7BY6GmQc7V7JiZf7swlGQsDhiPqKRA1fOXmqdWtIF
rmzCCSVo/FaneMqFeLn2wPAZyiFYOS3kcnI8LpbNBYjNsae+pt8YORUyo8Xi6q31
KqMCgYEA3SICcr+Hk7kvFXYw/XI6fAel9bgh7UenFnA/rG6xHnc9UjjLi56hpCaK
UykHJMj/JHCP5smpJoAZdjIDvVUVcj8HtvEgVsxAAwnv0vbA4iLILRROK9M08VO7
zrsaR7lPxeGz8R45hGMXf/JE3nr3K7wZkHyzYRxiPC9oaXzSlVQ=
-----END RSA PRIVATE KEY-----`;
}

function createPkcs8PrivateKey() {
  return `-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDX5XUFZinrSVk4
N6kCeRj/W8GkJ9OogFLb+M0uofIj2DXB3whidfuMMmi7ug9URRMWeWkAO97HE6kI
w0/gaWGjaxvVZqiPN/c2SGEfB9naLpkUbK1MobQ/yhveAXDsuB3Bu2t5dg2IADji
L+Gquit5LIp4boKvUpKofZe2VgRYCBTtE4cc31zyZ5+095un9cLvvF9EgzpI6Ftb
B1bCl9i9Le8iNJ27tcUAA636l+sm88ysuu6sSSuWDd3hpwP++cPtTQh9Moq/UV6N
NenBju8hRG+zT4kQfp9Ol96vkJ8wtjRD0d4NosI+6R2kEaFpIIgwDejaF2qF5iYh
BCL1kX7fAgMBAAECggEBAM4SpAcaRCrkYTfq0PSeWEDt+UBSD5LHoc97hYXcaH7U
UkOSOrOYwBlKyHNfnmAmapjKHHJvsy48Ut0daHHgxKSyiOfBzGSf2+TBY4q1+Lhs
QAuhzCzYMG+u37ihkYMyHp0b4vMVvsawx5sGbxWCDBrma+mWOkFUxM30y0nsgsru
coBSRboOoblOrS3qRZ3cR07LWn2XnDPY5RswK9YGfd07zY0wLy52teI6Eo1BduVt
xY51LSFoRY8k129DbJQ9rVCVrRyyeiJdJEelTftP2H/NXipgdjPUFO/Rli49JrY7
FtGMgo6vsLrA1sCDuYu/POxbr5l/FSF16Hc35wCkm3ECgYEA92QUDNIMXn8Zn4Jx
gWQ+Dm4eLKpHg37+3P/YQPxsDkvqrdCR8IlPrFl9XCxz8KIJm99Y731yB0ZY6hin
8tVcnmmMRpahEivhxrvRv4S/S2G/qLlAobf9zaOMlwnw2MaRcKKEQDL/XwN7HaZB
Wyq2qWWfwOYVU5POzIKe67ucsvkCgYEA32jN0gHlbEQeIUcjtgImTS5fLG5thPQm
OqOQpt6KOjksdRAuxjLi4VqwSbrGdmrA4thhUsBfZPDfPq8NFpcnb0I0feASOgty
zpmC4lG9hftAeaoLj9pnKBNQ9QF7Eg6UwromoL4TjOYtuQHiMM9a3FdPAk1mVJMx
QmTTevxw3pcCgYBZjdTkPzVTD9eKsoG8QTllp98sdAenRlWkUslJ3RaQplKx0QrD
dDFvRoXm1AC0XzPzRQrAxzx7/a86Luq9MRdyEOtCskxkc5YrhtoK5lcJRr1GqqkG
4b/yrP27v1F8Z4VGrM4LH2rqTixHM2P9SJ0O/UazYZbexmbaCX7V/vheoQJ/SG6y
TGRQ/ckX+7IU4e3QfTWLpdwnpfOCcYCWZYPBTR88Srf0ck4NQQMmJMFplvsFjoaZ
BztXsmJl/uzCUZCwOGI+opEDV85eap1a0gWubMIJJWj8Vqd4yoV4ufbA8BnKIVg5
LeRycjwuls0FiM2xp76m3xg5FTKjxeLqrfUqowKBgQDdIgJyv4eTuS8VdjD9cjp8
B6X1uCHtR6cWcD+sbrEedz1SOMuLnqGkJopTKQckyP8kcI/myakmgBl2MgO9VRVy
Pwe28SBWzEADCe/S9sDiIsgtFE4r0zTxU7vOuxpHuU/F4bPxHjmEYxd/8kTeevcr
vBmQfLNhHGI8L2hpfNKVVA==
-----END PRIVATE KEY-----`;
}
