import * as crypto from 'crypto';
import { readFile } from 'fs';
import ow from 'ow';
import { promisify } from 'util';

const readFileAsync = promisify(readFile);

import { CryptoKi } from './cryptoki';

export type LocalRsaOptions =
  | { privateKey: string }
  | { privateKeyPath: string };

export class LocalRsaCryptoKi implements CryptoKi {
  private privateKey?: string;
  private readonly options: LocalRsaOptions;

  constructor(options: LocalRsaOptions) {
    this.validateOptions(options);
    this.options = options;
  }

  private validateOptions(options: LocalRsaOptions): void {
    ow(
      options,
      'options',
      ow.object.exactShape({
        privateKey: ow.optional.string,
        privateKeyPath: ow.optional.string
      })
    );
    if (!(options as any).privateKey && !(options as any).privateKeyPath) {
      throw new Error(
        'Either property `privateKey` or `privateKeyPath` must be defined'
      );
    }
  }

  public async sign(data: string): Promise<string> {
    const sign = crypto.createSign('RSA-SHA256');
    sign.write(data);
    sign.end();
    const privateKey = await this.getPrivateKey();
    return sign.sign(privateKey, 'hex');
  }

  private getPrivateKey(): Promise<string> {
    if (this.privateKey) {
      return Promise.resolve(this.privateKey);
    }
    if ((this.options as any).privateKey) {
      return Promise.resolve((this.options as any).privateKey);
    }
    if ((this.options as any).privateKeyPath) {
      return readFileAsync((this.options as any).privateKeyPath, {
        encoding: 'UTF-8'
      });
    }
    return Promise.reject('No private key to sign');
  }
}
