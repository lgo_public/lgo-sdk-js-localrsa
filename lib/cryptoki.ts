export interface CryptoKi {
  sign: (data: string) => Promise<string>;
}
